package com.angrysnail.testcode.spring.configuration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Iterator;

public class Main {
    public static void main(String args[]) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(Main.class);
        Iterator<String> it = ((AnnotationConfigApplicationContext) ctx).getBeanFactory().getBeanNamesIterator();
        //org.springframework.context.annotation.internalConfigurationAnnotationProcessor
        //org.springframework.context.annotation.internalAutowiredAnnotationProcessor
        //org.springframework.context.annotation.internalCommonAnnotationProcessor
        //org.springframework.context.event.internalEventListenerProcessor
        //org.springframework.context.event.internalEventListenerFactory
        //main
        //environment
        //systemProperties
        //systemEnvironment
        //messageSource
        //applicationEventMulticaster
        //lifecycleProcessor
        while (it.hasNext()) {
            String key = it.next();
            System.out.println(key);
        }
        String string1 = (String) ctx.getBean("string1");
        String string2 = (String) ctx.getBean("string2");
        System.out.println(string1);
        System.out.println("============================");
        System.out.println(string2);
    }
}
