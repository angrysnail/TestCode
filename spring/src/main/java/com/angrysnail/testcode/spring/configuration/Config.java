package com.angrysnail.testcode.spring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public String string1() {
        return "string1";
    }

    @Bean
    public String string2() {
        return string1() + "_2";
    }
}
